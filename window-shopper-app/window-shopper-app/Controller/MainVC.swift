//
//  ViewController.swift
//  window-shopper-app
//
//  Created by Oforkanji Odekpe on 1/11/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet weak var wageTxt: CurrencyTextField!
    @IBOutlet weak var priceTxt: CurrencyTextField!
    @IBOutlet weak var resultLable: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let calcButton = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60 ))
        calcButton.backgroundColor = #colorLiteral(red: 1, green: 0.7388327823, blue: 0.220480338, alpha: 1)
        calcButton.setTitle("Calculate", for: .normal)
        calcButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        calcButton.addTarget(self, action: #selector(MainVC.calculate), for: .touchUpInside)
        
        wageTxt.inputAccessoryView = calcButton
        priceTxt.inputAccessoryView = calcButton
        
        resultLable.isHidden = true
        hoursLabel.isHidden = true
    }
    
    @objc func calculate(){
        if let wageTxt = wageTxt.text, let priceTxt = priceTxt.text{
            if let wage = Double(wageTxt), let price = Double(priceTxt){
                view.endEditing(true)
                resultLable.isHidden = false
                hoursLabel.isHidden = false
                resultLable.text = "\(Wage.getHours(forWage: wage, andPrice: price))"
            }
        }
    }
    @IBAction func clearCalculatorPressed(_ sender: Any) {
        resultLable.isHidden = true
        hoursLabel.isHidden = true
        wageTxt.text = " "
        priceTxt.text = " "
    }
    
}

