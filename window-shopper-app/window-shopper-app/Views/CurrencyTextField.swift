//
//  CurrencyTextField.swift
//  window-shopper-app
//
//  Created by Oforkanji Odekpe on 1/11/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import UIKit

@IBDesignable
//IBDesignable helps us to see the effects of the code on the interface builder
class CurrencyTextField: UITextField {
    
    override func draw(_ rect: CGRect) {
        let size: CGFloat = 20
        let currencyLablel = UILabel(frame: CGRect(x: 5, y: (frame.size.height / 2) - size / 2, width: size, height: size))
        currencyLablel.backgroundColor = #colorLiteral(red: 0.7703045685, green: 0.5175734709, blue: 0.1072315981, alpha: 0.7917647688)
        currencyLablel.textAlignment = .center
        currencyLablel.textColor = #colorLiteral(red: 0.1921568662, green: 0.007843137719, blue: 0.09019608051, alpha: 1)
        currencyLablel.layer.cornerRadius = 5.0
        currencyLablel.clipsToBounds = true
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = .current
        currencyLablel.text = formatter.currencySymbol
        addSubview(currencyLablel)
    }
    
    override func prepareForInterfaceBuilder() {
        super.awakeFromNib()
        custoizeView()
    }
    
    override func awakeFromNib() {
       custoizeView()
    }
    
    func custoizeView(){
        backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.2492508562)
        layer.cornerRadius = 5.0
        textAlignment = .center
        clipsToBounds = true
        if placeholder == nil {
            placeholder = " "
        }
        
        if let p = placeholder{
            let place = NSAttributedString(string: p, attributes: [.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)])
            attributedPlaceholder = place
            textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
}
