//
//  Wage.swift
//  window-shopper-app
//
//  Created by Oforkanji Odekpe on 1/12/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import Foundation

class Wage{
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int{
        return Int(ceil(price / wage))
        //ceil rounds up to the nearest whole number
        //round rounds up the nearest whole number if it's 5>=; rounds down if it's <=5
    }
}
